import os
from accountent.utils import create_accounts_from_txt

from . import forms


def getTotalConsideration(period_from, periode_till):
    total = 0.
    if os.environ.get('POSTGRES_HOST'):
        from hordak.models import Account
        balance_from = float(Account.objects.filter(full_code=3000).first().balance(as_of=period_from, raw=True))
        balance_till = float(Account.objects.filter(full_code=3000).first().balance(as_of=period_till, raw=True))
        total = balance_till - balance_from
    return total

def fillTurnoverComputationForm(period_from, periode_till):
    total_consideration = getTotalConsideration(period_from, periode_till)
    supplies_foreign = 0.
    supplies_abroad = 0.
    transfer_notification_procedure = 0.
    supplies_tax_exempt = 0.
    reduction_consideration = 0.

    initial_data = {
        "total_consideration": total_consideration,
        "supplies_foreign": supplies_foreign,
        "supplies_abroad": supplies_abroad,
        "transfer_notification_procedure": transfer_notification_procedure,
        "supplies_tax_exempt": supplies_tax_exempt,
        "reduction_consideration": reduction_consideration,
    }

    return initial_data #forms.TurnoverComputationForm(initial=initial_data)

def compute_step_from_data(data, step, initial_form={}):
    step_inital_form = initial_form
    if step == '1':
        step_inital_form = {**step_inital_form, **fillTurnoverComputationForm(data.get('period_from'), data.get('periode_till'))}


    return step_inital_form

def register_company(company):
    create_accounts_from_txt(filename=company.get('juridical_form'))
