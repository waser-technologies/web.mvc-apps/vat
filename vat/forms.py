from django import forms
from django.utils.translation import ugettext_lazy as _


from . import models


REPORTING_METHODS = (
    (1, _("Effective")),
    (2, _("Net tax rate")),
    (3, _("Flat tax rate"))
)

EXPORT_CHOICE = (
    (1, _("Compilation")),
    (2, _("Single")),
)

""" class UIDForm(forms.ModelForm):
    
    class Meta:
        model = models.UIDModel
        fields = ("organisation_id",) """

class VariousDeductionForm(forms.ModelForm):
    
    class Meta:
        model = models.VariousDeductionModel
        fields = ("amount", "description")


class TaxForm(forms.ModelForm):
    
    class Meta:
        model = models.TaxModel
        fields = ("rate", "turnover")

class CompanyForm(forms.ModelForm):
    
    class Meta:
        model = models.CompanyModel
        fields = ("name", "uid", "juridical_form")

class GeneralInformationForm(forms.ModelForm):
    
    class Meta:
        model = models.GeneralInformaitonModel
        fields = (
            "date_report_from",
            "date_report_till",
            "submission_type",
            "report_form",
            "business_ref_id",
            )

class TurnoverComputationForm(forms.ModelForm):
    
    class Meta:
        model = models.TurnoverComputationModel
        fields = (
            "total_consideration",
            "supplies_foreign",
            "supplies_abroad",
            "transfer_notification_procedure",
            "supplies_tax_exempt",
            "reduction_consideration",
#            "various_deduction",
            )

VariousDeductionFormset = forms.modelformset_factory(
        models.VariousDeductionModel,
        fields=(
            "amount",
            "description",
        )
    )

class ReportingMethodChoiceMethod(forms.Form):
    reporting_method = forms.ChoiceField(choices=REPORTING_METHODS)

class EffectiveReportingMethodForm1(forms.ModelForm):
    
    class Meta:
        model = models.EffectiveReportingMethodModel
        fields = (
            "gross_or_net",
            "opted",
            )

class EffectiveTaxRateForm(forms.ModelForm):

    class Meta:
        model = models.TaxModel
        fields = (
            'rate',
            'turnover',
        )

EffectiveSuppliesPerTaxRateFormset = forms.modelformset_factory(
        models.TaxModel,
        fields = (
            'rate',
            'turnover',
        )
    )

EffectiveAcquisitionTaxFormset = forms.modelformset_factory(
        models.TaxModel,
        fields = (
            'rate',
            'turnover',
        )
    )

class EffectiveReportingMethodForm2(forms.ModelForm):
    
    class Meta:
        model = models.EffectiveReportingMethodModel
        fields = (
            "input_tax_material_and_services",
            "input_tax_investments",
            "subsequent_input_tax_deduction",
            "input_tax_correction",
            "input_tax_reduction",
            )

class NetTaxRateForm(forms.ModelForm):
    
    class Meta:
        model = models.TaxModel
        fields = (
            'rate',
            'turnover',
        )

NetTaxSuppliesPerTaxRateFormset = forms.modelformset_factory(
        models.TaxModel,
        fields = (
            'rate',
            'turnover',
        )
    )

NetTaxAcquisitionTaxFormset = forms.modelformset_factory(
        models.TaxModel,
        fields = (
            'rate',
            'turnover',
        )
    )

class CompensationChoiceForm(forms.Form):
    choice = forms.ChoiceField(choices=EXPORT_CHOICE)

class VerificationCompensationExportForm(forms.ModelForm):

    class Meta:
        model = models.VerificationCompensationExportModel
        fields = (
            'invoice_number',
            'invoice_date',
            'type_of_service',
            'tax_rate',
            'turnover',
        )

CompilationCompensationExportFormset = forms.modelformset_factory(
        models.VerificationCompensationExportModel,
        fields=(
            'invoice_number',
            'invoice_date',
            'type_of_service',
            'tax_rate',
            'turnover',
        )
    )

class CompensationExportForm(forms.ModelForm):

    class Meta:
        model = models.NetTaxRateMethodModel
        fields = (
            'compensation_export',
        )

class DeemedInputTaxDeductionChoiceForm(forms.Form):
    choice = forms.ChoiceField(choices=EXPORT_CHOICE)

class VerificationDeemedInputTaxDeductionTurnoverAndTaxRateForm(forms.ModelForm):

    class Meta:
        model = models.TaxModel
        fields = (
            'rate',
            'turnover',
        )

class VerificationDeemedInputTaxDeductionMarginAndTaxRateForm(forms.ModelForm):

    class Meta:
        model = models.TaxModel
        fields = (
            'rate',
            'turnover',
        )

TurnoverCompilationDeemedInputTaxDeductionFormset = forms.modelformset_factory(
        models.TaxModel,
        fields = (
            'rate',
            'turnover',
        )
    )

MarginCompilationDeemedInputTaxDeductionFormset = forms.modelformset_factory(
        models.TaxModel,
        fields = (
            'rate',
            'turnover',
        )
    )

class DeemedInputTaxDeductionForm(forms.ModelForm):

    class Meta:
        model = models.NetTaxRateMethodModel
        fields = (
            'deemed_input_tax_deduction',
        )

class MarginTaxationChoiceForm(forms.Form):
    choice = forms.ChoiceField(choices=EXPORT_CHOICE)

class VerificationMarginTaxationTurnoverAndTaxRateForm(forms.ModelForm):

    class Meta:
        model = models.TaxModel
        fields = (
            'rate',
            'turnover',
        )

class VerificationMarginTaxationMarginAndTaxRateForm(forms.ModelForm):

    class Meta:
        model = models.TaxModel
        fields = (
            'rate',
            'turnover',
        )

TurnoverCompilationMarginTaxationFormset = forms.modelformset_factory(
        models.TaxModel,
        fields = (
            'rate',
            'turnover',
        )
    )

MarginCompilationMarginTaxationFormset = forms.formset_factory(VerificationMarginTaxationMarginAndTaxRateForm)

class MarginTaxationForm(forms.ModelForm):

    class Meta:
        model = models.NetTaxRateMethodModel
        fields = (
            'margin_taxation',
        )


class FlatTaxRateSuppliesForm(forms.ModelForm):
    
    class Meta:
        model = models.ActivityTaxModel
        fields = (
            'activity',
            'rate',
            'turnover',
        )

class FlatTaxRateAcquisitionForm(forms.ModelForm):
    
    class Meta:
        model = models.TaxModel
        fields = (
            'rate',
            'turnover',
        )

FlatTaxSuppliesPerTaxRateFormset = forms.modelformset_factory(
        models.ActivityTaxModel,
        fields=(
            'activity',
            'rate',
            'turnover',
        )
    )

FlatTaxAcquisitionTaxFormset = forms.modelformset_factory(
        models.TaxModel,
        fields=(
            'rate',
            'turnover',
        )
    )

class OtherFlowsOfFundsForm(forms.ModelForm):
    
    class Meta:
        model = models.OtherFlowsOfFundsModel
        fields = (
            "subsidies",
            "donations",
            )

class VatStatementForm(forms.ModelForm):
    
    class Meta:
        model = models.VatStatementModel
        fields = (
            "payable_tax",
            )

