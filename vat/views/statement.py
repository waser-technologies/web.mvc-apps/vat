from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView
from django.utils.translation import ugettext_lazy as _

import yaml

from .. import forms, utils
from ..models import VatStatementModel, GeneralInformaitonModel



class VatStatementModelListView(ListView):
    model = VatStatementModel
    template_name = "VAT/statement_list.html"


class GeneralInformationDetailView(UpdateView):
    model = GeneralInformaitonModel
    form_class = forms.GeneralInformationForm
    template_name = "VAT/statement/general_information.html"

def export_vat_statement_as_xml(request, statement_id):

    statementPath = "VAT/export/_eCH-0217.xml"
    generalInformationPath = "VAT/export/_eCH-0217.yml"

    f = open(generalInformationPath, 'r')
    context = yaml.load(f, Loader=yaml.CSafeLoader)
    f.close()

    statement = VatStatementModel.objects.get(pk=statement_id)

    context['generalInformation']['uid']['uidOrganisationIdCategorie'] = "CHE" #statement.general_information.uid
    context['generalInformation']['uid']['uidOrganisationId'] = str(statement.general_information.uid).replace('CHE-', "")

    context['generalInformation']['organisationName'] = statement.general_information.name
    context['generalInformation']['generationTime'] = statement.general_information.date_created
    context['generalInformation']['reportingPeriodFrom'] = statement.general_information.date_report_from
    context['generalInformation']['reportingPeriodTill'] = statement.general_information.date_report_till
    context['generalInformation']['typeOfSubmission'] = statement.general_information.submission_type
    context['generalInformation']['formOfReporting'] = statement.general_information.report_form
    context['generalInformation']['businessReferenceId'] = statement.general_information.business_ref_id

    context['turnoverComputation']['totalConsideration'] = statement.turnover_computation.total_consideration
    context['turnoverComputation']['suppliesToForeignCountries'] = statement.turnover_computation.supplies_foreign
    context['turnoverComputation']['suppliesAbroad'] = statement.turnover_computation.supplies_abroad
    context['turnoverComputation']['transferNotificationProcedure'] = statement.turnover_computation.transfer_notification_procedure
    context['turnoverComputation']['suppliesExemptFromTax'] = statement.turnover_computation.supplies_tax_exempt
    context['turnoverComputation']['reductionOfConsideration'] = statement.turnover_computation.reduction_consideration
    context['turnoverComputation']['variousDeduction'] = {}
    context['turnoverComputation']['variousDeduction']['amountVariousDeduction'] = statement.turnover_computation.various_deduction.amount
    context['turnoverComputation']['variousDeduction']['descriptionVariousDeduction'] = statement.turnover_computation.various_deduction.description

    context['effectiveReportingMethod']['grossOrNet'] = statement.effective_reporting_method.gross_or_net
    context['effectiveReportingMethod']['opted'] = statement.effective_reporting_method.opted
    context['effectiveReportingMethod']['suppliesPerTaxRate'] = {}
    context['effectiveReportingMethod']['suppliesPerTaxRate']['taxRate'] = statement.effective_reporting_method.supplies_per_tax_rate.rate
    context['effectiveReportingMethod']['suppliesPerTaxRate']['turnover'] = statement.effective_reporting_method.supplies_per_tax_rate.turnover
    context['effectiveReportingMethod']['acquisitionTax'] = {}
    context['effectiveReportingMethod']['acquisitionTax']['taxRate'] = statement.effective_reporting_method.acquisition_tax.rate
    context['effectiveReportingMethod']['acquisitionTax']['turnover'] = statement.effective_reporting_method.acquisition_tax.turnover
    context['effectiveReportingMethod']['inputTaxMaterialAndServices'] = statement.effective_reporting_method.input_tax_material_and_services
    context['effectiveReportingMethod']['inputTaxInvestments'] = statement.effective_reporting_method.input_tax_investments
    context['effectiveReportingMethod']['subsequentInputTaxDeduction'] = statement.effective_reporting_method.subsequent_input_tax_deduction
    context['effectiveReportingMethod']['inputTaxCorrections'] = statement.effective_reporting_method.input_tax_correction
    context['effectiveReportingMethod']['inputTaxReductions'] = statement.effective_reporting_method.input_tax_reduction

    context['payableTax'] = statement.payable_tax

    context['otherFlowsOfFunds'] = {}
    context['otherFlowsOfFunds']['subsidies'] = statement.other_flows_of_funds.subsidies
    context['otherFlowsOfFunds']['donations'] = statement.other_flows_of_funds.donations


    return render(request, statementPath, context, content_type='text/xml')