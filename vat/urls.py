import os
from django.conf.urls import url, include

from . import views

app_name = "vat"

urlpatterns = []

if os.environ.get('POSTGRES_HOST', False):
    #from hordak import views as hordak_views

    urlpatterns += [
        #url(r'^statement/create/$', views.VatStatementModelCreateView.as_view(), name="new-statement"),
        #url(r'^statement/edit/$', views.VatStatementModelUpdateView.as_view(), name="edit-statement"),
        #url(r'^statement/control/$', views.VatStatementWizard.as_view(), name="control-statement"),
        url(r'^statement/list/$', views.statement.VatStatementModelListView.as_view(), name="statement-list"),
    ]