from django.db import models
from django.utils.translation import ugettext_lazy as _

from .fields import VATField

STATEMENT_REPORT_TYPE = (
    (1, _("First deposit")),
    (2, _("Amending statement")),
    (3, _("Annual concordance")),
)

STATEMENT_REPORT_FORM = (
    (1, _("Agreed")),
    (2, _("Received")),
)

JURIDICAL_FORM_CHOICES = [
    ('inde', _("Independent")),
    ('sarl', _("Gmbh")),
    ('sa', _("AG")),
]

class CompanyModel(models.Model):
    """
    eCH-0097:uidStructureType
    eCH-0217:organisationNameType
    """
    uid = VATField(_('VAT No.'), unique=True, help_text=_("If your are a company based in Switzerland, enter your IDE number. (e.g CHE-123.456.789)"), blank=True)  #models.ForeignKey(UIDModel, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, verbose_name=_("Organisation name"), help_text=_("Registered company name"))
    juridical_form = models.CharField(max_length=4, choices=JURIDICAL_FORM_CHOICES, default=JURIDICAL_FORM_CHOICES[0][0])

    def __str__(self):
        return self.name

class GeneralInformaitonModel(models.Model):
    """
    ---
    xs:dateTime
    xs:date
    xs:date
    xs:int
    xs:int
    eCH-0058:businessReferenceIdType
    """
    company = models.ForeignKey(CompanyModel, on_delete=models.CASCADE)
    date_created = models.DateTimeField(auto_now_add=True)
    date_report_from = models.DateField(help_text=_("Start of the accounting period."))
    date_report_till = models.DateField(help_text=_("End of the accounting period."))
    submission_type = models.IntegerField(choices=STATEMENT_REPORT_TYPE, default=1)
    report_form = models.IntegerField(choices=STATEMENT_REPORT_FORM, default=1)
    business_ref_id = models.CharField(max_length=50, verbose_name=_("Case file ID"), help_text=_("This is your unique reference for this statement."))

    def __str__(self):
        return "GI:" + self.business_ref_id

class VariousDeductionModel(models.Model):
    amount = models.FloatField()
    description = models.CharField(max_length=50, verbose_name=_("Deduction description"))

    def __str__(self):
        return self.description + ":" + self.amount

class TurnoverComputationModel(models.Model):
    total_consideration = models.FloatField(help_text=_("Total consideration"))
    supplies_foreign = models.FloatField(blank=True, help_text=_("Supplies to foreign countries"))
    supplies_abroad = models.FloatField(blank=True, help_text=_("Supplies abroad."))
    transfer_notification_procedure = models.FloatField(blank=True)
    supplies_tax_exempt = models.FloatField(blank=True)
    reduction_consideration = models.FloatField(blank=True)
    various_deduction = models.ManyToManyField(VariousDeductionModel, blank=True)

class TaxModel(models.Model):
    rate = models.FloatField()
    turnover = models.FloatField()

GROSS_OR_NET_OPTIONS = (
    (1, _("Net")),
    (2, _("Gross")),
)

class EffectiveReportingMethodModel(models.Model):
    gross_or_net = models.IntegerField(choices=GROSS_OR_NET_OPTIONS, default=1)
    opted = models.FloatField(blank=True)
    supplies_per_tax_rate = models.ManyToManyField(TaxModel, related_name="effective_supplies_per_tax_rate", blank=True)
    acquisition_tax = models.ManyToManyField(TaxModel, related_name="effective_acquisition_tax", blank=True)
    input_tax_material_and_services = models.FloatField(blank=True)
    input_tax_investments = models.FloatField(blank=True)
    subsequent_input_tax_deduction = models.FloatField(blank=True)
    input_tax_correction = models.FloatField(blank=True)
    input_tax_reduction = models.FloatField(blank=True)


class VerificationCompensationExportModel(models.Model):
    invoice_number = models.CharField(max_length=80, verbose_name=_("Client invoice No."))
    invoice_date = models.DateField()
    type_of_service = models.CharField(max_length=100, verbose_name=_("Type of serice"), help_text=_("Type of object or service."))
    tax_rate = models.FloatField()
    turnover = models.FloatField(verbose_name=_("Turnover"), help_text=_("This must always be a gross number."))

class CompilationCompensationExportModel(models.Model):
    verification_compensation_export = models.ManyToManyField(VerificationCompensationExportModel, related_name="verification_compensation_export")

class VerificationDeemedInputTaxDeductionModel(models.Model):
    turnover_and_tax_rate = models.ForeignKey(TaxModel, on_delete=models.CASCADE, related_name="verification_deemed_input_tax_deduciton_turnover_and_tax_rate")
    margin_and_tax_rate = models.ForeignKey(TaxModel, on_delete=models.CASCADE, related_name="verification_deemed_input_tax_deduciton_margin_and_tax_rate")

class CompilationDeemedInputTaxDeductionModel(models.Model):
    verification_deemed_input_tax_deduction = models.ManyToManyField(VerificationDeemedInputTaxDeductionModel, related_name="verification_deemed_input_tax_deduction")

class VerificationMarginTaxationModel(models.Model):
    turnover_and_tax_rate = models.ForeignKey(TaxModel, on_delete=models.CASCADE, related_name="verification_margin_taxation_turnover_and_tax_rate")
    margin_and_tax_rate = models.ForeignKey(TaxModel, on_delete=models.CASCADE, related_name="verification_margin_taxation_margin_and_tax_rate")

class CompilationMarginTaxationModel(models.Model):
    verification_margin_taxation = models.ManyToManyField(VerificationMarginTaxationModel, related_name="verification_margin_taxation")

class NetTaxRateMethodModel(models.Model):
    supplies_per_tax_rate = models.ManyToManyField(TaxModel, related_name="net_tax_supplies_per_tax_rate", blank=True)
    acquisition_tax = models.ManyToManyField(TaxModel, related_name="net_tax_acquisition_tax", blank=True)
    
    compilation_compensation_export = models.ForeignKey(CompilationCompensationExportModel, on_delete=models.CASCADE, related_name="net_tax_compilation_compensation_export", blank=True)
    compensation_export = models.FloatField(blank=True)
    
    compilation_deemed_input_tax_deduction = models.ForeignKey(CompilationDeemedInputTaxDeductionModel, on_delete=models.CASCADE, related_name="net_tax_compilation_deemed_input_tax_deduction", blank=True)
    deemed_input_tax_deduction = models.FloatField(blank=True)
    
    compilation_margin_taxation = models.ForeignKey(CompilationMarginTaxationModel, on_delete=models.CASCADE, related_name="net_tax_compilation_margin_taxation", blank=True)
    margin_taxation = models.FloatField(blank=True)

class ActivityTaxModel(models.Model):
    activity = models.CharField(max_length=100)
    rate = models.FloatField()
    turnover = models.FloatField()

class FlatTaxRateMethodModel(models.Model):
    supplies_per_tax_rate = models.ManyToManyField(ActivityTaxModel, related_name="flat_tax_supplies_per_tax_rate", blank=True)
    acquisition_tax = models.ManyToManyField(TaxModel, related_name="flat_tax_acquisition_tax", blank=True)
    
    compilation_compensation_export = models.ForeignKey(CompilationCompensationExportModel, on_delete=models.CASCADE, related_name="flat_tax_compilation_compensation_export", blank=True)
    compensation_export = models.FloatField(blank=True)
    
    compilation_deemed_input_tax_deduction = models.ForeignKey(CompilationDeemedInputTaxDeductionModel, on_delete=models.CASCADE, related_name="flat_tax_compilation_deemed_input_tax_deduction", blank=True)
    deemed_input_tax_deduction = models.FloatField(blank=True)
    
    compilation_margin_taxation = models.ForeignKey(CompilationMarginTaxationModel, on_delete=models.CASCADE, related_name="flat_tax_compilation_margin_taxation", blank=True)
    margin_taxation = models.FloatField(blank=True)

class OtherFlowsOfFundsModel(models.Model):
    subsidies = models.FloatField(blank=True)
    donations = models.FloatField(blank=True)

class VatStatementModel(models.Model):
    general_information = models.ForeignKey(GeneralInformaitonModel, on_delete=models.CASCADE)
    turnover_computation = models.ForeignKey(TurnoverComputationModel, on_delete=models.CASCADE)
    
    effective_reporting_method = models.ForeignKey(EffectiveReportingMethodModel, on_delete=models.CASCADE, blank=True)
    net_tax_rate_method = models.ForeignKey(NetTaxRateMethodModel, on_delete=models.CASCADE, blank=True)
    flat_tax_rate_method = models.ForeignKey(FlatTaxRateMethodModel, on_delete=models.CASCADE, blank=True)

    payable_tax = models.FloatField()
    other_flows_of_funds = models.ForeignKey(OtherFlowsOfFundsModel, on_delete=models.CASCADE)

    def __str__(self):
        return "VAT: " + self.general_information.company.uid


