from django.shortcuts import redirect
from formtools.wizard.views import SessionWizardView
from django.utils.translation import ugettext_lazy as _

from .. import forms

def effective_reporting_method_condition(wizard):
    cleaned_data = wizard.get_cleaned_data_for_step('3') or {}
    if cleaned_data.get('reporting_method') == '1':
        return True
    else:
        return False

def net_tax_reporting_method_condition(wizard):
    cleaned_data = wizard.get_cleaned_data_for_step('3') or {}
    if cleaned_data.get('reporting_method') == '2':
        return True
    else:
        return False

def flat_tax_reporting_method_condition(wizard):
    cleaned_data = wizard.get_cleaned_data_for_step('3') or {}
    if cleaned_data.get('reporting_method') == '3':
        return True
    else:
        return False

def not_effective_reporting_method_condition(wizard):
    cleaned_data = wizard.get_cleaned_data_for_step('3') or {}
    print(cleaned_data.get('reporting_method'))
    if cleaned_data.get('reporting_method') != '1':
        return True
    else:
        return False

wizard_extra_context = {
    '0': {'step_name': _("General information")},
    '1': {'step_name': _("Turnover")},
    '2': {'step_name': _("Various deduction")},
    '3': {'step_name': _("Reporting method")},
    '4': {'step_name': _("Effective reporting")},
    '5': {'step_name': _("Effective reporting - Supplies pet tax rate")},
    '6': {'step_name': _("Effective reporting - Acquisition tax")},
    '7': {'step_name': _("Effective reporting 2")},
    '8': {'step_name': _("Net tax reporting - Supplies pet tax rate")},
    '9': {'step_name': _("Net tax reporting - Acquisition tax")},
    '10': {'step_name': _("Flat tax - Supplies pet tax rate")},
    '11': {'step_name': _("Flat tax - Acquisition tax")},
    '12': {'step_name': _("Compensation export")},
    '13': {'step_name': _("Compensation export")},
    '14': {'step_name': _("Compilation compensation export")},
    '15': {'step_name': _("Deemed input taxation deduction")},
    '16': {'step_name': _("Deemed input taxation deduction - Turnover taxation compilation")},
    '17': {'step_name': _("Deemed input taxation deduction - Margin taxation compilation")},
    '18': {'step_name': _("Deemed input taxation deduction")},
    '19': {'step_name': _("Margin taxation")},
    '20': {'step_name': _("Margin taxation - Turnover taxation compilation")},
    '21': {'step_name': _("Margin taxation - Marigin taxation compilation")},
    '22': {'step_name': _("Margin taxation")},
    '23': {'step_name': _("Payable tax")},
    '24': {'step_name': _("Other flow of funds")},
}

statement_wizard_form_list = [
#        forms.UIDForm,
#        forms.CompanyForm,
        forms.GeneralInformationForm, #0
        forms.TurnoverComputationForm, #1
        forms.VariousDeductionFormset, #2
        forms.ReportingMethodChoiceMethod, #3
        forms.EffectiveReportingMethodForm1, #4
        forms.EffectiveSuppliesPerTaxRateFormset, #5
        forms.EffectiveAcquisitionTaxFormset, #6
        forms.EffectiveReportingMethodForm2, #7
        
        forms.NetTaxSuppliesPerTaxRateFormset, #8
        forms.NetTaxAcquisitionTaxFormset, #9

        forms.FlatTaxSuppliesPerTaxRateFormset, #10
        forms.FlatTaxAcquisitionTaxFormset, #11

        forms.CompensationChoiceForm, #12
        forms.CompensationExportForm, #13
        forms.CompilationCompensationExportFormset, #14

        forms.DeemedInputTaxDeductionChoiceForm, #15
        forms.TurnoverCompilationDeemedInputTaxDeductionFormset, #16
        forms.MarginCompilationDeemedInputTaxDeductionFormset, #17
        forms.DeemedInputTaxDeductionForm, #18
        
        forms.MarginTaxationChoiceForm, #19
        forms.TurnoverCompilationMarginTaxationFormset, #20
        forms.MarginCompilationMarginTaxationFormset, #21
        forms.MarginTaxationForm, #22

        forms.VatStatementForm, #23
        forms.OtherFlowsOfFundsForm, #24
    ]

class VatStatementWizard(SessionWizardView):
    template_name = "VAT/wizard/statement_wizard.html"
    condition_dict = {
        '4': effective_reporting_method_condition,
        '5': effective_reporting_method_condition,
        '6': effective_reporting_method_condition,
        '7': effective_reporting_method_condition,
        '8': net_tax_reporting_method_condition,
        '9': net_tax_reporting_method_condition,
        '10': flat_tax_reporting_method_condition,
        '11': flat_tax_reporting_method_condition,
        '12': not_effective_reporting_method_condition,
        '13': not_effective_reporting_method_condition,
        '14': not_effective_reporting_method_condition,
        '15': not_effective_reporting_method_condition,
        '16': not_effective_reporting_method_condition,
        '17': not_effective_reporting_method_condition,
        '18': not_effective_reporting_method_condition,
        '19': not_effective_reporting_method_condition,
        '20': not_effective_reporting_method_condition,
        '21': not_effective_reporting_method_condition,
        '22': not_effective_reporting_method_condition,
    }

    form_list = statement_wizard_form_list

    def get_context_data(self, form, **kwargs):
        """
        Supply extra content data for each stage.
        e.g. stage page title
        """
        context = super(VatStatementWizard, self).get_context_data(form, **kwargs)
        context.update(
            wizard_extra_context.get(str(self.steps.current))
        )
        return context

    def done(self, form_list, **kwargs):
        #do_something_with_the_form_data(form_list)
        return redirect('accountent:statement-list')
